/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <stdlib.h>

#include "./dataloop.h"

static void DLOOP_Type_indexed_array_copy(DLOOP_Count count,
                                          DLOOP_Count contig_count,
                                          const DLOOP_Size *input_blocklength_array,
                                          const void *input_displacement_array,
                                          DLOOP_Size *output_blocklength_array,
                                          DLOOP_Offset *out_disp_array,
                                          int dispinbytes,
                                          DLOOP_Offset old_extent,
                                          int *all_aligned,
                                          int *is_short);


/*@
  DLOOP_Dataloop_create_indexed

  Arguments:
  +  int icount
  .  DLOOP_Size *iblocklength_array
  .  void *displacement_array (either ints or MPI_Aints)
  .  int dispinbytes
  .  MPI_Datatype oldtype
  .  DLOOP_Dataloop **dlp_p
  .  int *dlsz_p
  .  int *dldepth_p
  -  int flag

  .N Errors
  .N Returns 0 on success, -1 on error.
  @*/

int PREPEND_PREFIX(Dataloop_create_indexed)(DLOOP_Count count,
                                            const DLOOP_Size *blocklength_array,
                                            const void *displacement_array,
                                            int dispinbytes,
                                            MPI_Datatype type,
                                            MPI_Datatype oldtype,
                                            DLOOP_Dataloop **dl,
                                            MPI_Aint *size,
                                            int *depth)
{
    if(!*dl) {
        MPIU_Assert(*depth == -1);
        PREPEND_PREFIX(Dataloop_alloc)(dl);
        *depth = 1;
    }

    int err, is_builtin;
    MPI_Aint i;
    DLOOP_Size blksz;
    DLOOP_Count first;
    int all_aligned = 1, is_short = 1;

    DLOOP_Count old_type_count = 0, contig_count;
    DLOOP_Offset old_size, old_extent;

    /* if count is zero, handle with contig code, call it an int */
    if (count == 0)
    {
        err = PREPEND_PREFIX(Dataloop_create_contiguous)(0,
                                                         MPI_INT,
                                                         dl,
                                                         size,
                                                         depth);
        return err;
    }

    /* Skip any initial zero-length blocks */
    for (first = 0; first < count; first++)
        if ((DLOOP_Count) blocklength_array[first])
            break;
    

    is_builtin = (DLOOP_Handle_hasloop_macro(oldtype)) ? 0 : 1;
    DLOOP_Handle_get_size_macro(oldtype, old_size);
    DLOOP_Handle_get_extent_macro(oldtype, old_extent);

    for (i=first; i < count; i++)
    {
        old_type_count += (DLOOP_Count) blocklength_array[i];
    }

    contig_count = PREPEND_PREFIX(Type_indexed_count_contig)(count,
                                                             blocklength_array,
                                                             displacement_array,
                                                             dispinbytes,
                                                             old_extent);

    /* if contig_count is zero (no data), handle with contig code */
    if (contig_count == 0)
    {
        err = PREPEND_PREFIX(Dataloop_create_contiguous)(0,
                                                         MPI_INT,
                                                         dl,
                                                         size,
                                                         depth);
        return err;
    }

    /* optimization:
     *
     * if contig_count == 1 and block starts at displacement 0,
     * store it as a contiguous rather than an indexed dataloop.
     */
    if ((contig_count == 1) &&
        ((!dispinbytes && ((int *) displacement_array)[first] == 0) ||
         (dispinbytes && ((MPI_Aint *) displacement_array)[first] == 0)))
    {
        err = PREPEND_PREFIX(Dataloop_create_contiguous)(old_type_count,
                                                         oldtype,
                                                         dl,
                                                         size,
                                                         depth);
        return err;
    }

    /* optimization:
     *
     * if contig_count == 1 (and displacement != 0), store this as
     * a single element blockindexed rather than a lot of individual
     * blocks.
     */
    if (contig_count == 1)
    {
        const void *disp_arr_tmp; /* no ternary assignment to avoid clang warnings */
        if (dispinbytes)
            disp_arr_tmp = &(((const MPI_Aint *)displacement_array)[first]);
        else
            disp_arr_tmp = &(((const int *)displacement_array)[first]);
        err = PREPEND_PREFIX(Dataloop_create_blockindexed)(1,
                                                           old_type_count,
                                                           disp_arr_tmp,
                                                           dispinbytes,
                                                           type,
                                                           oldtype,
                                                           dl,
                                                           size,
                                                           depth);
        return err;
    }

    /* optimization:
     *
     * if block length is the same for all blocks, store it as a
     * blockindexed rather than an indexed dataloop.
     */
    blksz = blocklength_array[first];
    for (i = first+1; i < count; i++)
    {
        if (blocklength_array[i] != blksz)
        {
            blksz--;
            break;
        }
    }
    if (blksz == blocklength_array[first])
    {
        const void *disp_arr_tmp; /* no ternary assignment to avoid clang warnings */
        if (dispinbytes)
            disp_arr_tmp = &(((const MPI_Aint *)displacement_array)[first]);
        else
            disp_arr_tmp = &(((const int *)displacement_array)[first]);
        err = PREPEND_PREFIX(Dataloop_create_blockindexed)(count-first,
                                                           blksz,
                                                           disp_arr_tmp,
                                                           dispinbytes,
                                                           type,
                                                           oldtype,
                                                           dl,
                                                           size,
                                                           depth);

        return err;
    }

    /* note: blockindexed looks for the vector optimization */

    /* TODO: optimization:
     *
     * if an indexed of a contig, absorb the contig into the blocklen array
     * and keep the same overall depth
     */

    /* otherwise storing as an indexed dataloop */

    MPI_Aint typesize, typeextent;
    DLOOP_Handle_get_size_macro(type, typesize);
    DLOOP_Handle_get_extent_macro(type, typeextent);

    MPID_Datatype *oldtype_ptr;
    MPID_Datatype_get_ptr(oldtype, oldtype_ptr);

    (*dl)[*depth].kind = DL_INDEX;
    (*dl)[*depth].count  = contig_count;
    (*dl)[*depth].size   = typesize;
    (*dl)[*depth].extent = typeextent;

    (*dl)[*depth].s.i_t.oldsize = old_size;
    /* copy in displacement parameters
     *
     * regardless of dispinbytes, we store displacements in bytes in loop.
     */
    DLOOP_Offset *tmpdisps = (DLOOP_Offset*)DLOOP_Malloc((contig_count) * sizeof(DLOOP_Offset));
    DLOOP_Size *tmpblks = (DLOOP_Size*)DLOOP_Malloc((contig_count) * sizeof(DLOOP_Size));

    DLOOP_Type_indexed_array_copy(count,
                                  contig_count,
                                  blocklength_array,
                                  displacement_array,
                                  tmpblks,
                                  tmpdisps,
                                  dispinbytes,
                                  old_extent,
                                  &all_aligned,
                                  &is_short);
    (*dl)[*depth].s.i_t.offsets = tmpdisps; 
    (*dl)[*depth].s.i_t.blklens = tmpblks;

    if(!is_builtin) {
        PREPEND_PREFIX(Dataloop_create)(oldtype, &oldtype_ptr->dataloop, &oldtype_ptr->dataloop_size, &oldtype_ptr->dataloop_depth);
    }
    
    if(is_builtin || DLOOP_Dataloop_is_contig(oldtype_ptr->dataloop)) {
        (*dl)[*depth].kind = DL_INDEXFINAL;
        if(all_aligned)
            DLOOP_opt_set_aligned((*dl)[*depth]);
        if(is_short) 
            DLOOP_opt_set_isshort((*dl)[*depth]);

        *depth += 1;
        (*dl)[*depth].kind = DL_CONTIGFINAL;
        /* We don't use the size and extent for contigfinal in the interpreted
         * code, but we need this data in the JITted version */
        (*dl)[*depth].size = old_size;
        (*dl)[*depth].extent = old_extent;
        (*dl)[*depth].s.c_t.basesize = old_size;
        (*dl)[*depth].s.c_t.baseextent = old_extent;
    } else {
        *depth += 1;
        (*dl)[*depth].kind = DL_CONTIGCHILD;
        (*dl)[*depth].size = old_size;
        (*dl)[*depth].extent = typeextent;
        int i;
        /* TODO: TLP: Use MPI_Aint max or something */
        MPI_Aint min = LONG_MAX;
        for(i = 0; i < contig_count; i++) 
            if(tmpdisps[i] < min)
                min = tmpdisps[i];
        (*dl)[*depth].size = min;

        *depth += 1;
        /* The first element of the innerdl will be DL_EXIT */
        DLOOP_Dataloop *pos = &(*dl)[*depth];
        PREPEND_PREFIX(Dataloop_dup)(&oldtype_ptr->dataloop[1], &pos);
        /* The last element will be DL_BOTTOM */
        *depth = *depth + oldtype_ptr->dataloop_depth - 1;
    }

    return MPI_SUCCESS;
}

/* DLOOP_Type_indexed_array_copy()
 *
 * Copies arrays into place, combining adjacent contiguous regions and
 * dropping zero-length regions.
 *
 * Extent passed in is for the original type.
 *
 * Output displacements are always output in bytes, while block
 * lengths are always output in terms of the base type.
 */
static void DLOOP_Type_indexed_array_copy(DLOOP_Count count,
                                          DLOOP_Count contig_count,
                                          const DLOOP_Size *in_blklen_array,
                                          const void *in_disp_array,
                                          DLOOP_Size *out_blklen_array,
                                          DLOOP_Offset *out_disp_array,
                                          int dispinbytes,
                                          DLOOP_Offset old_extent,
                                          int *all_aligned,
                                          int *is_short)
{
    DLOOP_Count i, first, cur_idx = 0;
    *all_aligned = 1;
    *is_short = 1;

    /* Skip any initial zero-length blocks */
    for (first = 0; first < count; ++first)
        if ((DLOOP_Count) in_blklen_array[first])
            break;

    out_blklen_array[0] = (DLOOP_Count) in_blklen_array[first];

    if (!dispinbytes)
    {
        DLOOP_Offset offset0 = ((int*)in_disp_array)[first] * old_extent;
        out_disp_array[0] = offset0;
        switch(old_extent) {
        case 8:
        case 4:
        case 2:
            if(offset0 % old_extent != 0)
                *all_aligned = 0;
            break;
        default:
            *all_aligned = 0;
            break;
        }
	
        for (i = first+1; i < count; ++i)
        {
            if (in_blklen_array[i] == 0)
            {
                continue;
            }
            else if (out_disp_array[cur_idx] +
                     ((DLOOP_Offset) out_blklen_array[cur_idx]) * old_extent ==
                     ((DLOOP_Offset) ((int*) in_disp_array)[i]) * old_extent)
            {
                /* adjacent to current block; add to block */
                out_blklen_array[cur_idx] += (DLOOP_Count) in_blklen_array[i];
            }
            else
            {
                DLOOP_Offset offset = ((int*)in_disp_array)[i] * old_extent;
                cur_idx++;
                DLOOP_Assert(cur_idx < contig_count);
                out_disp_array[cur_idx] = offset;
                out_blklen_array[cur_idx] = in_blklen_array[i];
                if(in_blklen_array[i] > DLOOP_MEMCPY_THRESHOLD)
                    *is_short = 0;
                switch(old_extent) {
                case 8:
                case 4:
                case 2:
                    if(offset % old_extent != 0)
                        *all_aligned = 0;
                    break;
                default:
                    *all_aligned = 0;
                    break;
                }
            }
            
            
        }
    }
    else /* input displacements already in bytes */
    {
        out_disp_array[0] = (DLOOP_Offset) ((MPI_Aint *) in_disp_array)[first];
	
        for (i = first+1; i < count; ++i)
        {
            if (in_blklen_array[i] == 0)
            {
                continue;
            }
            else if (out_disp_array[cur_idx] +
                     ((DLOOP_Offset) out_blklen_array[cur_idx]) * old_extent ==
                     ((DLOOP_Offset) ((MPI_Aint *) in_disp_array)[i]))
            {
                /* adjacent to current block; add to block */
                out_blklen_array[cur_idx] += in_blklen_array[i];
            }
            else
            {
                DLOOP_Offset offset = ((MPI_Aint*)in_disp_array)[i];
                cur_idx++;
                DLOOP_Assert(cur_idx < contig_count);
                out_disp_array[cur_idx] = offset;
                out_blklen_array[cur_idx] = (DLOOP_Size) in_blklen_array[i];
                if(in_blklen_array[i] > DLOOP_MEMCPY_THRESHOLD)
                    *is_short = 0;
                switch(old_extent) {
                case 8:
                case 4:
                case 2:
                    if(offset % old_extent != 0)
                        *all_aligned = 0;
                    break;
                default:
                    *all_aligned = 0;
                    break;
                }
            }
        }
    }

    DLOOP_Assert(cur_idx == contig_count - 1);
    return;
}

/* DLOOP_Type_indexed_count_contig()
 *
 * Determines the actual number of contiguous blocks represented by the
 * blocklength/displacement arrays.  This might be less than count (as
 * few as 1).
 *
 * Extent passed in is for the original type.
 */
DLOOP_Count PREPEND_PREFIX(Type_indexed_count_contig)(DLOOP_Count count,
                                                      const DLOOP_Size *blocklength_array,
                                                      const void *displacement_array,
                                                      int dispinbytes,
                                                      DLOOP_Offset old_extent)
{
    DLOOP_Count i, contig_count = 1;
    DLOOP_Count cur_blklen, first;

    if (count)
    {
        /* Skip any initial zero-length blocks */
        for (first = 0; first < count; ++first)
            if ((DLOOP_Count) blocklength_array[first])
                break;

        if (first == count) { /* avoid invalid reads later on */
            contig_count = 0;
            return contig_count;
        }

        cur_blklen = (DLOOP_Count) blocklength_array[first];
        if (!dispinbytes)
        {
            DLOOP_Offset cur_tdisp =
                (DLOOP_Offset) ((int *) displacement_array)[first];
	
            for (i = first+1; i < count; ++i)
            {
                if (blocklength_array[i] == 0)
                {
                    continue;
                }
                else if (cur_tdisp + (DLOOP_Offset) cur_blklen ==
                         (DLOOP_Offset) ((int *) displacement_array)[i])
                {
                    /* adjacent to current block; add to block */
                    cur_blklen += (DLOOP_Count) blocklength_array[i];
                }
                else
                {
                    cur_tdisp  = (DLOOP_Offset) ((int *) displacement_array)[i];
                    cur_blklen = (DLOOP_Count) blocklength_array[i];
                    contig_count++;
                }
            }
        }
        else
        {
            DLOOP_Offset cur_bdisp =
                (DLOOP_Offset) ((MPI_Aint *) displacement_array)[first];
	
            for (i = first+1; i < count; ++i)
            {
                if (blocklength_array[i] == 0)
                {
                    continue;
                }
                else if (cur_bdisp + (DLOOP_Offset) cur_blklen * old_extent ==
                         (DLOOP_Offset) ((MPI_Aint *) displacement_array)[i])
                {
                    /* adjacent to current block; add to block */
                    cur_blklen += (DLOOP_Count) blocklength_array[i];
                }
                else
                {
                    cur_bdisp  =
                        (DLOOP_Offset) ((MPI_Aint *) displacement_array)[i];
                    cur_blklen = (DLOOP_Count) blocklength_array[i];
                    contig_count++;
                }
            }
        }
    }
    return contig_count;
}
