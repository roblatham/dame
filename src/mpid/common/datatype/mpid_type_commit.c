/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <mpid_dataloop.h>
#include <stdlib.h>

/*@
  MPID_Type_commit
 
Input Parameters:
. datatype_p - pointer to MPI datatype

Output Parameters:

  Return Value:
  0 on success, -1 on failure.
@*/

int MPID_Type_commit(MPI_Datatype *datatype_p)
{
    int           mpi_errno=MPI_SUCCESS;
    MPID_Datatype *datatype_ptr;

    MPIU_Assert(HANDLE_GET_KIND(*datatype_p) != HANDLE_KIND_BUILTIN);

    MPID_Datatype_get_ptr(*datatype_p, datatype_ptr);

    if (datatype_ptr->is_committed == 0) {
       datatype_ptr->is_committed = 1;

       MPID_Dataloop_create(*datatype_p, &datatype_ptr->dataloop, &datatype_ptr->size, &datatype_ptr->dataloop_depth);
        /* If there are any structs, it will adjust the inner types */
        PREPEND_PREFIX(Dataloop_update)(datatype_ptr->dataloop, 0);

#if 0
        PREPEND_PREFIX(Dataloop_print)(datatype_ptr->dataloop);
#endif

#ifdef WITH_JIT
        void *ee = DLJIT_compile(datatype_ptr->dataloop);
        if(ee) {
            datatype_ptr->dljit_ee = ee;
            datatype_ptr->dljit_fn_pack = DLJIT_function_pack(ee);
            datatype_ptr->dljit_fn_unpack = DLJIT_function_unpack(ee);
        } 
#endif
#ifdef MPID_Dev_datatype_commit_hook
       MPID_Dev_datatype_commit_hook(datatype_p);
#endif /* MPID_Dev_datatype_commit_hook */

    }
    return mpi_errno;
}

