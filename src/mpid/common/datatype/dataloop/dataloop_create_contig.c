/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include "./dataloop.h"

/*@
  Dataloop_contiguous - create the dataloop representation for a
  contiguous datatype

  Arguments:
  +  int icount,
  .  MPI_Datatype oldtype,
  .  DLOOP_Dataloop **dlp_p,
  .  int *dlsz_p,
  .  int *dldepth_p,
  -  int flag

  .N Errors
  .N Returns 0 on success, -1 on failure.
  @*/
int PREPEND_PREFIX(Dataloop_create_contiguous)(DLOOP_Count count,
                                               DLOOP_Type oldtype,
                                               DLOOP_Dataloop **dl,
                                               MPI_Aint *size,
                                               int *depth)
{
    if(!*dl) {
        MPIU_Assert(*depth == -1);
        PREPEND_PREFIX(Dataloop_alloc)(dl);
        *depth = 1;
    }

    int is_builtin;
    is_builtin = (DLOOP_Handle_hasloop_macro(oldtype)) ? 0 : 1;

    DLOOP_Size oldsize = 0, oldextent = 0;
    DLOOP_Handle_get_size_macro(oldtype, oldsize);
    DLOOP_Handle_get_extent_macro(oldtype, oldextent);

    (*dl)[*depth].kind   = DL_CONTIG;
    (*dl)[*depth].count  = count;
    (*dl)[*depth].size   = count * oldsize;
    (*dl)[*depth].extent = count * oldextent;
    (*dl)[*depth].s.c_t.basesize = oldsize;
    (*dl)[*depth].s.c_t.baseextent = oldextent;

    if (is_builtin) {
        (*dl)[*depth].kind = DL_CONTIGFINAL;
        (*dl)[*depth].size = count * oldsize;
        (*dl)[*depth].extent = count * oldextent;
    } else {
        MPID_Datatype *old_ptr;
        MPID_Datatype_get_ptr(oldtype, old_ptr);

        PREPEND_PREFIX(Dataloop_create)(oldtype, 
                                        &old_ptr->dataloop,
                                        &old_ptr->dataloop_size,
                                        &old_ptr->dataloop_depth);

        if((old_ptr->dataloop[1].kind == DL_CONTIGFINAL)
           && (oldsize == oldextent)) {
            (*dl)[*depth].kind = DL_CONTIGFINAL;
            (*dl)[*depth].size = count * oldsize;
            (*dl)[*depth].extent = count * oldextent;
        } else {
            if(count > 1) {
                *depth += 1;
                DLOOP_Dataloop *pos = &(*dl)[*depth];
                PREPEND_PREFIX(Dataloop_dup)(&old_ptr->dataloop[1], &pos);
                *depth = *depth + old_ptr->dataloop_depth - 1;
            } else {
                DLOOP_Dataloop *pos = &(*dl)[*depth];
                PREPEND_PREFIX(Dataloop_dup)(&old_ptr->dataloop[1], &pos);
                *depth = *depth + old_ptr->dataloop_depth;
            }
        }
    } 

    *depth += 1;
    (*dl)[*depth].kind = DL_BOTTOM;

    return 0;
}
