/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "./dataloop.h"

#undef DEBUG_DLOOP_SIZE
#undef DLOOP_DEBUG_MEMORY

/* Dataloops
 *
 * The functions here are used for the creation, copying, update, and display
 * of DLOOP_Dataloop structures and trees of these structures.
 *
 * Currently we store trees of dataloops in contiguous regions of memory.  They
 * are stored in such a way that subtrees are also stored contiguously.  This
 * makes it somewhat easier to copy these subtrees around.  Keep this in mind
 * when looking at the functions below.
 *
 * The structures used in this file are defined in mpid_datatype.h.  There is
 * no separate mpid_dataloop.h at this time.
 *
 * OPTIMIZATIONS:
 *
 * There are spots in the code with OPT tags that indicate where we could
 * optimize particular calculations or avoid certain checks.
 *
 * NOTES:
 *
 * Don't have locks in place at this time!
 */

/* Some functions in this file are responsible for allocation of space for
 * dataloops.  These structures include the dataloop structure itself
 * followed by a sequence of variable-sized arrays, depending on the loop
 * kind.  For example, a dataloop of kind DLOOP_KIND_INDEXED has a
 * dataloop structure followed by an array of block sizes and then an array
 * of offsets.
 *
 * For efficiency and ease of cleanup (preserving a single free at
 * deallocation), we want to allocate this memory as a single large chunk.
 * However, we must perform some alignment of the components of this chunk
 * in order to obtain correct and efficient operation across all platforms.
 */


/*@
  Dataloop_free - deallocate the resources used to store a dataloop

Input/output Parameters:
  . dataloop - pointer to dataloop structure
  @*/
void PREPEND_PREFIX(Dataloop_free)(DLOOP_Dataloop **dl)
{

    if (*dl == NULL) return;

#ifdef DLOOP_DEBUG_MEMORY
    DLOOP_dbg_printf("DLOOP_Dataloop_free: freeing loop @ %x.\n",
                     (int) *dl);
#endif
    unsigned i = 0, j = 0;
    for(i = 0; i < DLOOP_MAX_DATATYPE_DEPTH; i++) {
        switch((*dl)[i].kind) {
        case DL_BLOCKINDEX:
        case DL_BLOCKINDEX1:
        case DL_BLOCKINDEXFINAL:
            memset((void*)((*dl)[i].s.i_t.offsets), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.bi_t.offsets));
            (*dl)[i].s.bi_t.offsets = NULL;
            break;
        case DL_INDEX:
        case DL_INDEXFINAL: 
            memset((void*)((*dl)[i].s.i_t.offsets), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.i_t.offsets));
            (*dl)[i].s.i_t.offsets = NULL;
            memset((void*)((*dl)[i].s.i_t.blklens), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.i_t.blklens));
            (*dl)[i].s.i_t.blklens = NULL;
            break;
        case DL_STRUCT:
            memset((void*)((*dl)[i].s.s_t.oldsizes), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.s_t.oldsizes));
            memset((void*)((*dl)[i].s.s_t.blklens), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.s_t.blklens));
            memset((void*)((*dl)[i].s.i_t.offsets), 0, (*dl)[i].count*sizeof(MPI_Aint));
            DLOOP_Free((void*)((*dl)[i].s.s_t.offsets));
            (*dl)[i].s.s_t.offsets = NULL;
            for(j = 0; j < (*dl)[i].count; j++) {
                PREPEND_PREFIX(Dataloop_free)(&((*dl)[i].s.s_t.dls[j]));
                (*dl)[i].s.s_t.dls[j] = NULL;
            }
            DLOOP_Free((void*)((*dl)[i].s.s_t.dls));
        default:
            break;
        }
    }

    memset(*dl, 0, DLOOP_MAX_DATATYPE_DEPTH * sizeof(DLOOP_Dataloop));
    DLOOP_Free(*dl);
    *dl = NULL;
    return;
}

/*@
  Dataloop_alloc - allocate the resources used to store a dataloop with
  no old loops associated with it.

  Input Parameters:
  + kind          - kind of dataloop to allocate
  . count         - number of elements in dataloop (kind dependent)
  . new_loop_p    - address at which to store new dataloop pointer
  - new_loop_sz_p - pointer to integer in which to store new loop size

  Notes:
  The count parameter passed into this function will often be different
  from the count passed in at the MPI layer due to optimizations.
  @*/
void PREPEND_PREFIX(Dataloop_alloc)(DLOOP_Dataloop **new_dl)
{
    /* TODO: TLP: Error checking */
    *new_dl = (DLOOP_Dataloop*)DLOOP_Malloc(DLOOP_MAX_DATATYPE_DEPTH * sizeof(DLOOP_Dataloop));
    unsigned i = 0;
    (*new_dl)[0].kind = DL_EXIT;
    for(i = 1; i < DLOOP_MAX_DATATYPE_DEPTH; i++) {
        (*new_dl)[i].kind = DL_BOTTOM;
        (*new_dl)[i].count = 0;
        (*new_dl)[i].flags = 0x0;
    }
    return;
}

void PREPEND_PREFIX(Dataloop_struct_alloc)(DLOOP_Count count,
                                           DLOOP_Dataloop *dl)
{
    dl->kind = DL_STRUCT;
    dl->s.s_t.dls = (DLOOP_Dataloop**)MPIU_Malloc(count*sizeof(DLOOP_Dataloop*));
    unsigned i;
    for(i = 0; i < count; i++)
        PREPEND_PREFIX(Dataloop_alloc)(&dl->s.s_t.dls[i]);
}

/*@
  Dataloop_dup - make a copy of a dataloop

  Returns 0 on success, -1 on failure.
  @*/
void PREPEND_PREFIX(Dataloop_dup)(DLOOP_Dataloop *old_dl,
                                  DLOOP_Dataloop **new_dl)
{
    if(*new_dl == 0)
        PREPEND_PREFIX(Dataloop_alloc)(new_dl);
    int i, j;
    for(i = 0; i < DLOOP_MAX_DATATYPE_DEPTH && old_dl[i].kind != DL_BOTTOM; i++) {
        (*new_dl)[i].kind = old_dl[i].kind;
        (*new_dl)[i].count = old_dl[i].count;
        (*new_dl)[i].size = old_dl[i].size;
        (*new_dl)[i].extent = old_dl[i].extent;
        (*new_dl)[i].returnto = old_dl[i].returnto;
        switch(old_dl[i].kind) {
        case DL_CONTIG:
        case DL_CONTIGFINAL:
            (*new_dl)[i].s.c_t.basesize = old_dl[i].s.c_t.basesize;
            (*new_dl)[i].s.c_t.baseextent = old_dl[i].s.c_t.baseextent;
            break;
        case DL_VECTOR:
        case DL_VECTOR1:
        case DL_VECTORFINAL:
            (*new_dl)[i].s.v_t.oldsize = old_dl[i].s.v_t.oldsize;
            (*new_dl)[i].s.v_t.blklen = old_dl[i].s.v_t.blklen;
            (*new_dl)[i].s.v_t.stride = old_dl[i].s.v_t.stride;
            break;
        case DL_BLOCKINDEX:
        case DL_BLOCKINDEX1:
        case DL_BLOCKINDEXFINAL:
            (*new_dl)[i].s.bi_t.oldsize = old_dl[i].s.bi_t.oldsize;
            (*new_dl)[i].s.bi_t.blklen = old_dl[i].s.bi_t.blklen;
            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.bi_t.offsets = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.bi_t.offsets, 
                   old_dl[i].s.bi_t.offsets, 
                   old_dl[i].count*sizeof(MPI_Aint));
            break;
        case DL_INDEX:
        case DL_INDEXFINAL:
            (*new_dl)[i].s.i_t.oldsize = old_dl[i].s.i_t.oldsize;
            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.i_t.blklens = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.i_t.blklens, 
                   old_dl[i].s.i_t.blklens, 
                   old_dl[i].count*sizeof(MPI_Aint));

            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.i_t.offsets = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.i_t.offsets, 
                   old_dl[i].s.i_t.offsets, 
                   old_dl[i].count*sizeof(MPI_Aint));
            break;
        case DL_STRUCT:
            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.s_t.oldsizes = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.s_t.oldsizes, 
                   old_dl[i].s.s_t.oldsizes, 
                   old_dl[i].count*sizeof(MPI_Aint));

            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.s_t.blklens = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.s_t.blklens, 
                   old_dl[i].s.s_t.blklens, 
                   old_dl[i].count*sizeof(MPI_Aint));

            /* TODO: TLP: Error checking */
            (*new_dl)[i].s.s_t.offsets = (MPI_Aint*)MPIU_Malloc(old_dl[i].count * sizeof(MPI_Aint));
            memcpy((MPI_Aint*)(*new_dl)[i].s.s_t.offsets, 
                   old_dl[i].s.s_t.offsets, 
                   old_dl[i].count*sizeof(MPI_Aint));

            PREPEND_PREFIX(Dataloop_struct_alloc)(old_dl[i].count, &(*new_dl)[i]);
            for(j = 0; j < old_dl[i].count; j++)
                PREPEND_PREFIX(Dataloop_dup)(old_dl[i].s.s_t.dls[j],
                                             &(*new_dl)[i].s.s_t.dls[j]);
            break;
        case DL_CONTIGCHILD:
            break;
        case DL_EXIT:
            break;
        case DL_BOTTOM:
            goto end;
            break;
        default:
            break;
        }
    }
 end:
    return;
}


/* --BEGIN ERROR HANDLING-- */
/*@
  Dataloop_print - dump a dataloop tree to stdout for debugging
  purposes

  Input Parameters:
  + dataloop - root of tree to dump
  - depth - starting depth; used to help keep up with where we are in the tree
  @*/
void PREPEND_PREFIX(Dataloop_print)(struct DLOOP_Dataloop *dl)
{
    int i, j;
    int sp = 0;
    do {
        switch (dl[sp].kind) {
        case DL_CONTIG:
            fprintf(stderr, "CONTIG(%d): %ld, %ld, %ld\n\t%ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent,
                    (long)dl[sp].s.c_t.basesize, (long)dl[sp].s.c_t.baseextent);
            break;
        case DL_VECTOR:
            fprintf(stderr, "VEC(%d): %ld, %ld, %ld\n\t%ld, %ld\n\t%ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent, 
                    (long)dl[sp].s.v_t.blklen, (long)dl[sp].s.v_t.oldsize,
                    (long)dl[sp].s.v_t.stride );
            break;
        case DL_VECTOR1:
            fprintf(stderr, "VEC1(%d): %ld, %ld, %ld\n\t%ld\n\t%ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent, 
                    (long)dl[sp].s.v_t.oldsize,
                    (long)dl[sp].s.v_t.stride );
            break;
        case DL_BLOCKINDEX:
            fprintf(stderr, "BLKINDEX(%d): %ld, %ld, %ld\n", 
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent);
            fprintf(stderr, "\t%ld, %ld\n", 
                    (long)dl[sp].s.bi_t.blklen, (long)dl[sp].s.bi_t.oldsize);
            fprintf(stderr, "\t%ld ", dl[sp].s.bi_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.bi_t.offsets[i]);
            fprintf( stderr, "\n" );
            break;
        case DL_BLOCKINDEX1:
            fprintf(stderr, "BLKINDEX1(%d): %ld, %ld, %ld\n", 
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent);
            fprintf(stderr, "\t%ld\n", (long)dl[sp].s.bi_t.oldsize);
            fprintf(stderr, "\t%ld ", dl[sp].s.bi_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.bi_t.offsets[i]);
            fprintf( stderr, "\n" );
            break;
        case DL_INDEX:
            fprintf(stderr, "INDEX(%d): %ld, %ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent );
            fprintf(stderr, "\t%ld", dl[sp].s.i_t.oldsize);
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.blklens[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.blklens[i]);
            fprintf( stderr, "\n" );
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.offsets[i]);
            fprintf( stderr, "\n" );
            break;
        case DL_VECTORFINAL:
            fprintf(stderr, "VECFINAL(%d): %ld, %ld, %ld\n\t%ld, %ld\n\t%ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent, 
                    (long)dl[sp].s.v_t.blklen, (long)dl[sp].s.v_t.oldsize, 
                    (long)dl[sp].s.v_t.stride );
            break;
        case DL_BLOCKINDEXFINAL:
            fprintf(stderr, "BLKINDEXFINAL(%d): %ld, %ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent);
            fprintf( stderr, "\t%ld, %ld\n", 
                     (long)dl[sp].s.bi_t.blklen, (long)dl[sp].s.bi_t.oldsize);
            fprintf(stderr, "\t%ld ", dl[sp].s.bi_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.bi_t.offsets[i]);
            fprintf( stderr, "\n" );
            break;
        case DL_CONTIGFINAL:
            fprintf(stderr, "CONTIGFINAL(%d): %ld, %ld, %ld\n\t%ld, %ld\n", sp, 
                    (long)dl[sp].count, (long)dl[sp].size, (long)dl[sp].extent,
                    (long)dl[sp].s.c_t.basesize, (long)dl[sp].s.c_t.baseextent);
            break;
        case DL_INDEXFINAL:
            fprintf(stderr, "INDEXFINAL(%d): %ld, %ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent );
            fprintf(stderr, "\t%ld", dl[sp].s.i_t.oldsize);
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.blklens[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.blklens[i]);
            fprintf( stderr, "\n" );
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.offsets[i]);
            fprintf( stderr, "\n" );
            break;
        case DL_CONTIGCHILD:
            fprintf(stderr, "CONTIGCHILD(%d): %ld, %ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent );
            break;
        case DL_STRUCT:
            fprintf(stderr, "STRUCT(%d): %ld, %ld, %ld\n",
                    sp,
                    (long)dl[sp].count, (long)dl[sp].size,
                    (long)dl[sp].extent);
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.blklens[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.blklens[i]);
            fprintf( stderr, "\n" );
            fprintf(stderr, "\t%ld ", dl[sp].s.i_t.offsets[0]);
            for(i = 1; i < dl[sp].count; i++)
                fprintf(stderr, "%ld ", dl[sp].s.i_t.offsets[i]);
            fprintf( stderr, "\n" );
            for(j = 0; j < dl[sp].count; j++) {
                fprintf(stderr, " --- %d --- \n", j);
                PREPEND_PREFIX(Dataloop_print)(dl[sp].s.s_t.dls[j]);
                fprintf(stderr, "\n");
            }
            break;
        case DL_RETURNTO:
            fprintf(stderr, "RETURNTO(%d): %d\n", 
                    sp,
                    dl[sp].returnto);
            break;
        case DL_EXIT: 
            fprintf(stderr, "EXIT(%d)\n", sp);
            break;
        case DL_BOTTOM: 
            fprintf(stderr, "BOTTOM(%d)\n", sp);
            break;
        default:
            fprintf(stderr, "UNKNOWN(%d)\n", sp);
            break;
        }
        sp++;
    } while(dl[sp].kind != DL_BOTTOM && sp < DLOOP_MAX_DATATYPE_DEPTH);
    fprintf(stderr, "BOTTOM(%d)\n", sp);

}
/* --END ERROR HANDLING-- */

void PREPEND_PREFIX(Dataloop_move)(DLOOP_Dataloop *old, DLOOP_Dataloop *new) {
    int i, j;
    for(j = 0; j < DLOOP_MAX_DATATYPE_DEPTH && old[j].kind != DL_BOTTOM; )
        j++;
    j++;
    DLOOP_Dataloop *tmp;
    PREPEND_PREFIX(Dataloop_alloc)(&tmp);
    memcpy(tmp, old, j*sizeof(DLOOP_Dataloop));
    memcpy(new, tmp, j*sizeof(DLOOP_Dataloop));
    MPIU_Free(tmp);
}

void PREPEND_PREFIX(Dataloop_update)(DLOOP_Dataloop *dl, DLOOP_Offset depth) {
    int i, j;
    DLOOP_Dataloop *tmp = 0;
    for(i = 0; i < DLOOP_MAX_DATATYPE_DEPTH && dl[i].kind != DL_BOTTOM; i++) {
        switch(dl[i].kind) {
        case DL_RETURNTO:
            if(dl[i].returnto != depth) {
                dl[i].returnto = depth;
                PREPEND_PREFIX(Dataloop_move)(&dl[i], &dl[depth]);
                for(j = i; j < depth; j++)
                    dl[j].kind = DL_EXIT;
            }
            break;
                
        case DL_STRUCT:
            for(j = 0; j < dl[i].count; j++) {
                PREPEND_PREFIX(Dataloop_update)(dl[i].s.s_t.dls[j], i);
            }
            break;
        default:
            break;
        }
    }
        
}
