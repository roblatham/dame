/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include "veccpy.h"
#include "./dataloop.h"

#undef DLOOP_DEBUG_MANIPULATE

#ifndef PREPEND_PREFIX
#error "You must explicitly include a header that sets the PREPEND_PREFIX and includes dataloop_parts.h"
#endif

#define DBG_PRINTF(s, ...)                      \
/* #define DBG_PRINTF fprintf */

static void DL_pack_contig(const void *in, void *out, DLOOP_Dataloop *dl, MPI_Aint count) {
    DLOOP_Offset basesize = dl->s.c_t.basesize;
    if(count > DLOOP_MEMCPY_THRESHOLD) {
        DLOOP_Memcpy(out, in, count);
    } else {
        DLOOP_Offset full = count / basesize;
        DLOOP_Offset part = count % basesize;
        switch(basesize) {
        case 8:
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int64_t, full);
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int8_t, part);
            break;
        case 4:
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int32_t, full);
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int8_t, part);
            break;
        case 2:
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int16_t, full);
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int8_t, part);
            break;
        default:
            MPIDI_COPY_BLOCK_UNALIGNED(in, out, int8_t, count);
            break;
        }
    }
}

static void DL_pack_vector(const void *in, void **out, 
                           DLOOP_Dataloop *dl,
                           MPI_Aint count)
{
    const char* restrict src = (const char*) in;
    char* restrict dst = (char*)*out;
    MPI_Aint i;

    MPI_Aint stride = dl->s.v_t.stride;
    MPI_Aint blklen = dl->s.v_t.blklen;
    MPI_Aint oldsize = dl->s.v_t.oldsize;
    MPI_Aint blksize = blklen*oldsize;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl); 

    /* The alignment part of the code doesn't seem to make any difference here
     * so we're probably better off ditching these runtime tests */
    switch(oldsize) {
    case 8: 
        MPIDI_COPY_FROM_VEC_ALIGNED(src,dst, stride/8, int64_t, blklen,count,8);
        break;
    case 4:
        MPIDI_COPY_FROM_VEC_ALIGNED(src,dst, stride/4, int32_t, blklen,count,4);
        break;
    case 2:
        MPIDI_COPY_FROM_VEC_ALIGNED(src,dst, stride/2, int16_t, blklen,count,2);
        break;
    default:
        if(isshort) {
            for(i = 0; i < count; i++) {
                MPIDI_COPY_BLOCK_UNALIGNED(src, dst, int8_t, blklen*oldsize);
                dst += (blklen*oldsize);
                src += stride;
            }
        } else {
            for(i = 0; i < count; i++) {
                DLOOP_Memcpy(dst, src, blklen*oldsize);
                dst += (blklen*oldsize);
                src += stride;
            }
        }
        break;
    }
    *out = (void*)((char*)*out + count*blklen*oldsize);
}

static void DL_pack_blockindexed(const void *in, void **out, 
                                 DLOOP_Dataloop *dl,
                                 int begin, int end)
{
    const char* restrict src = (const char*) in;
    char* restrict dst = (char*)*out;
    MPI_Aint i;

    MPI_Aint blklen = dl->s.bi_t.blklen;
    MPI_Aint oldsize = dl->s.bi_t.oldsize;
    const MPI_Aint *offsets = dl->s.bi_t.offsets;
    MPI_Aint blksize = blklen*oldsize;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl);

    const char* restrict tmpsrc = 0;
    switch(oldsize) {
    case 8:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int64_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    case 4:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int32_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    case 2:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int16_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    default:
        if(isshort) {
            for(i = begin; i < end; i++) {
                tmpsrc = src + offsets[i];
                MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int8_t, blklen*oldsize);
                dst += (blklen*oldsize);
            }
        } else { 
            for(i = begin; i < end; i++) {
                tmpsrc = src + offsets[i];
                DLOOP_Memcpy(dst, tmpsrc, blklen*oldsize);
                dst += (blklen*oldsize);
            }
        }
        break;
    }
    *out = (void*)dst;
}

static void DL_pack_indexed(const void *in, void **out, 
                            DLOOP_Dataloop *dl,
                            int begin, int end)
{
    const char* restrict src = (const char*) in;
    char* restrict dst = (char*)*out;
    MPI_Aint i;

    MPI_Aint blklen;
    MPI_Aint oldsize = dl->s.i_t.oldsize;
    const MPI_Aint *blklens = dl->s.i_t.blklens;
    const MPI_Aint *offsets = dl->s.i_t.offsets;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl);

    const char* restrict tmpsrc = 0;
    switch(oldsize) {
    case 8:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int64_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    case 4:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int32_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    case 2:
        for(i = begin; i < end; i++) {
            tmpsrc = src + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpsrc, dst, int16_t, blklen);
            dst += blklen*oldsize;
        }
        break;
    default:
        if(isshort) {
            for(i = begin; i < end; i++) {
                tmpsrc = src + offsets[i];
                blklen = blklens[i];
                MPIDI_COPY_BLOCK_UNALIGNED(src, dst, int8_t, blklen*oldsize);
                dst += (blklen*oldsize);
            }
        } else { 
            for(i = begin; i < end; i++) {
                tmpsrc = src + offsets[i];
                blklen = blklens[i];
                DLOOP_Memcpy(dst, tmpsrc, blklen*oldsize);
                dst += (blklen*oldsize);
            }
        }
        break;
    }
    *out = (void*)dst;
}


static void DL_unpack_vector(const void **in, void *out, 
                             DLOOP_Dataloop *dl,
                             MPI_Aint count)
{
    const char* restrict src = (const char*)*in;
    char* restrict dst = (char*)out;
    MPI_Aint i;

    MPI_Aint stride = dl->s.v_t.stride;
    MPI_Aint blklen = dl->s.v_t.blklen;
    MPI_Aint oldsize = dl->s.v_t.oldsize;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl);

    switch(oldsize) {
    case 8:
        MPIDI_COPY_TO_VEC_ALIGNED(src,dst, stride/8, int64_t, blklen, count, 8);
        break;
    case 4:
        MPIDI_COPY_TO_VEC_ALIGNED(src,dst, stride/4, int32_t, blklen, count, 4);
        break;
    case 2:
        MPIDI_COPY_TO_VEC_ALIGNED(src,dst, stride/2, int16_t, blklen, count, 2);
        break;
    default:
        if(isshort) {
            for(i = 0; i < count; i++) {
                MPIDI_COPY_BLOCK_UNALIGNED(src, dst, int8_t, blklen*oldsize);
                src += (blklen*oldsize);
                dst += stride;
            } 
        } else {
            for(i = 0; i < count; i++) {
                memcpy(dst, src, blklen*oldsize);
                src += (blklen*oldsize);
                dst += stride;
            }
        }
        break;
    }
    *in = (void*)((char*)*in + count*blklen*oldsize);
}

static void DL_unpack_blockindexed(const void **in, void *out, 
                                   DLOOP_Dataloop *dl,
                                   int begin, int end)
{
    const char* restrict src = (const char*)*in;
    char* restrict dst = (char*)out;
    MPI_Aint i;

    MPI_Aint blklen = dl->s.bi_t.blklen;
    MPI_Aint oldsize = dl->s.bi_t.oldsize;
    const MPI_Aint *offsets = dl->s.bi_t.offsets;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl);

    char* restrict tmpdst = 0;
    switch(oldsize) {
    case 8:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int64_t, blklen);
            src += blklen*oldsize;
        }
        break;
    case 4:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int32_t, blklen);
            src += blklen*oldsize;
        }
        break;
    case 2:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int16_t, blklen);
            src += blklen*oldsize;
        }
        break;
    default:
        if(isshort) {
            for(i = begin; i < end; i++) {
                tmpdst = dst + offsets[i];
                MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int8_t, blklen*oldsize);
                src += (blklen*oldsize);
            }
        } else { 
            for(i = begin; i < end; i++) {
                tmpdst = dst + offsets[i];
                DLOOP_Memcpy(tmpdst, src, blklen*oldsize);
                src += (blklen*oldsize);
            }
        }
        break;
    }
    *in = (void*)src;
}

static void DL_unpack_indexed(const void **in, void *out, 
                              DLOOP_Dataloop *dl,
                              int begin, int end)
{
    const char* restrict src = (const char*)*in;
    char* restrict dst = (char*)out;
    MPI_Aint i;

    MPI_Aint blklen;
    MPI_Aint oldsize = dl->s.i_t.oldsize;
    const MPI_Aint *blklens = dl->s.i_t.blklens;
    const MPI_Aint *offsets = dl->s.i_t.offsets;

    int aligned = DLOOP_opt_get_aligned(*dl);
    int isshort = DLOOP_opt_get_isshort(*dl);

    char* restrict tmpdst = 0;
    switch(oldsize) {
    case 8:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int64_t, blklen);
            src += blklen*oldsize;
        }
        break;
    case 4:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(src, tmpdst, int32_t, blklen);
            src += blklen*oldsize;
        }
        break;
    case 2:
        for(i = begin; i < end; i++) {
            tmpdst = dst + offsets[i];
            blklen = blklens[i];
            MPIDI_COPY_BLOCK_UNALIGNED(tmpdst, src, int16_t, blklen);
            src += blklen*oldsize;
        }
        break;
    default:
        if(isshort) {
            for(i = begin; i < end; i++) {
                tmpdst = dst + offsets[i];
                blklen = blklens[i];
                MPIDI_COPY_BLOCK_UNALIGNED(tmpdst, src, int8_t, blklen*oldsize);
                src += blklen*oldsize;
            }
        } else {
            for(i = begin; i < end; i++) {
                tmpdst = dst + offsets[i];
                blklen = blklens[i];
                DLOOP_Memcpy(tmpdst, src, blklen*oldsize);
                src += blklen*oldsize;
            }
        }
        break;
    }
    *in = (void*)src;
}


static void dlStackCopy(long sp, DLOOP_Dataloop *dl, MPI_Aint partial, DLOOP_Dataloop_state *state)
{
    state->sp = sp;
    state->partial = partial;
    state->dl = dl;
}

/*
  Return values:
  -1: Error
  0: Done
  1: Needs to continue; state in state_p
*/
int DL_pack(const void *inPtr, void*outPtr, DLOOP_Offset outSize, 
            DLOOP_Dataloop *dl,
            DLOOP_Dataloop_state *state,
            DLOOP_Offset *copy_p,
            int isbuiltin)
{
    MPI_Offset inbase = (MPI_Offset)inPtr;
    MPI_Offset outbase = (MPI_Offset)outPtr;
    long sp = 0;
    MPI_Aint sizeleft = outSize;

    /* Local variables */
    DLOOP_Dataloop_stackelm *stack = state->stack;
    /* If it's a resume, this will get set correctly later */
    DLOOP_Offset partial = 0;
    
    *copy_p = 0;

    /* This is a resume */
    if(state->sp != 0) {
        sp      = state->sp;
        inbase  = state->stack[sp].base;
        partial = state->partial;

        /* NOTE: For builtin types, we create a dataloop as needed on the stack
           so we don't save it in the state because when we resume, the same 
           type will be recreated in segment_packunpack. */
        if(!isbuiltin)
            dl  = state->dl;

        /* By construction, the last state pushed into the stack will always
         * be a CONTIGFINAL. Need to decrement the stack pointer to ensure 
         * that it is set to the right place by the immediate next call to 
         * dlpush */
        sp--;
        goto dlpush;
    }
  
    do {
    dlpush:
        sp++;
        switch (dl[sp].kind) {
        case DL_CONTIG: {
            DBG_PRINTF(stderr, "contig%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            goto dlpush;
        }
        case DL_VECTOR: {
            DBG_PRINTF(stderr, "vec%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = inbase;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            goto dlpush;
            break;
        }
        case DL_VECTOR1: {
            DBG_PRINTF(stderr, "vec%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX: {
            DBG_PRINTF(stderr, "blkidx%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = inbase;
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            inbase               += dl[sp].s.bi_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX1: {
            DBG_PRINTF(stderr, "blkidx%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            inbase               += dl[sp].s.bi_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_INDEX: {
            DBG_PRINTF(stderr, "idx%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = inbase;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[0];
            inbase               += dl[sp].s.i_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_STRUCT: {
            DBG_PRINTF(stderr, "struct%d_push(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp].prevdl      = dl;
            inbase               += dl[sp].s.s_t.offsets[0];
            dl                    = dl[sp].s.s_t.dls[0];
            goto dlpush;
            break;
        }
        case DL_CONTIGCHILD: {
            DBG_PRINTF(stderr, "cc%d_push\n",
                       sp);
            /* countLeft set by parent */
            /* inbase set by parent */
            goto dlpush;
        }
        case DL_VECTORFINAL: {
            DBG_PRINTF(stderr, "vf%d_push(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeleft >= dl[sp].size) {
                DL_pack_vector((const void*)inbase, 
                               (void**)&outbase, 
                               &dl[sp],
                               dl[sp].count);
                stack[sp].countLeft = 0;
                sizeleft -= dl[sp].size;
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
                break; /* to dlpop */
            }
            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count = sizeleft / (dl[sp].s.v_t.blklen * dl[sp].s.v_t.oldsize);
            if (count > 0) {
                DL_pack_vector((const void*)inbase, 
                               (void**)&outbase, 
                               &dl[sp],
                               count);
                sizeleft -= count * dl[sp].s.v_t.blklen * dl[sp].s.v_t.oldsize;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            inbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEXFINAL: {
            DBG_PRINTF(stderr, "bf%d_push(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeleft >= dl[sp].size) {
                DL_pack_blockindexed((const void*)stack[sp].base,
                                     (void**)&outbase, 
                                     &dl[sp],
                                     0, 
                                     dl[sp].count);
                stack[sp].countLeft = 0;
                sizeleft -= dl[sp].size;
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
                break;
            }
            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count = sizeleft / (dl[sp].s.bi_t.blklen * dl[sp].s.bi_t.oldsize);
            if (count > 0) {
                DL_pack_blockindexed((const void*)stack[sp].base, 
                                     (void**)&outbase, 
                                     &dl[sp],
                                     0, 
                                     count);
                sizeleft -= count*dl[sp].s.bi_t.blklen*dl[sp].s.bi_t.oldsize;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count;
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            inbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEXFINAL: {
            DBG_PRINTF(stderr, "if%d_push(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            stack[sp].base        = inbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeleft >= dl[sp].size) {
                DL_pack_indexed((const void*)stack[sp].base,
                                (void**)&outbase, 
                                &dl[sp],
                                0, 
                                dl[sp].count);
                stack[sp].countLeft = 0;
                sizeleft -= dl[sp].size;
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }

            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count, copied;
            for(count = 0, copied = 0; 
                count < dl[sp].count 
                    && copied + dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize <= sizeleft; 
                count++)
                copied += dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize;
                
            if (count > 0) {
                DL_pack_indexed((const void*)stack[sp].base,
                                (void**)&outbase, 
                                &dl[sp],
                                0, 
                                count);
                sizeleft -= copied;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            inbase = stack[sp].base + dl[sp].s.i_t.offsets[idx];
            goto dlpush;
            break;
        }

        case DL_CONTIGFINAL: {
            DBG_PRINTF(stderr, "cf%d_push(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);

            stack[sp].base = inbase;

            /* Quick check to see if there's enough buffer space to copy. 
             * sizeleft can be zero if the previous *FINAL type has blksize 
             * which is a multiple of in/outsize */
            if(sizeleft == 0) {
                dlStackCopy(sp, dl, 0, state);
                *copy_p = outSize;
                return 1;
            }

            /* This case occurs when the contigfinal is directly below a contig
             * which is part of a struct. There ought to be a better way of
             * doing this, and eventually, I'll try and get rid of this check */
            if(stack[sp].countLeft == 0 && partial == 0) 
                stack[sp].countLeft = dl[sp].count;

            DLOOP_Offset basesize = dl[sp].s.c_t.basesize;
            /* partial contains the remainder of an element that has been 
               partially packed
               countLeft contains the number of complete blocks left */
            DLOOP_Offset tocopy = stack[sp].countLeft*basesize + partial;
            if(tocopy > sizeleft) {
                DL_pack_contig((const void*)inbase,
                               (void*)outbase,
                               &dl[sp],
                               sizeleft);
                DLOOP_Offset newLeft = stack[sp].countLeft;
                if(partial >= sizeleft) {
                    partial -= sizeleft;
                } else {
                    DLOOP_Offset avail = sizeleft-partial;
                    newLeft -= (avail/basesize);
                    partial = 0;
                    if(avail%basesize) {
                        newLeft -= 1;
                        partial = basesize - avail%basesize;
                    }
                }
                stack[sp].base      = inbase + sizeleft;
                stack[sp].countLeft = newLeft;
                dlStackCopy(sp, dl, partial, state);
                *copy_p = outSize;
                return 1;
            } else {
                DL_pack_contig((const void*)inbase,
                               (void*)outbase,
                               &dl[sp],
                               tocopy);
                partial   = 0;
                inbase   += tocopy;
                outbase  += tocopy;
                sizeleft -= tocopy;
                stack[sp].countLeft = 0;
                goto dlpop;
            }
            break;
        }
        default: {
            /* This can never happen */
            DBG_PRINTF(stderr, "Unknown dataloop kind(%d)!\n", dl[sp].kind);
            MPIU_Assert(0);
            return -1;
        }
        }
    dlpop:
        sp--;

        switch (dl[sp].kind) {
        case DL_CONTIG: {
            DBG_PRINTF(stderr, "contig%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if(--stack[sp].countLeft == 0)
                goto dlpop; 
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + idx*dl[sp].s.c_t.baseextent;
            goto dlpush;
            break;
        }
        case DL_VECTOR: {
            DBG_PRINTF(stderr, "vec%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            /* For vectors, we don't need to compute offsets from the base.
             * The stride is the distance from each element, so we can use
             * the stack contents to update */
            if(--stack[sp].countLeft == 0) 
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            goto dlpush;
            break;
        }
        case DL_VECTOR1: {
            DBG_PRINTF(stderr, "vec%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX: {
            DBG_PRINTF(stderr, "blkidx%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX1: {
            DBG_PRINTF(stderr, "blkidx%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEX: {
            DBG_PRINTF(stderr, "idx%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if(--stack[sp].countLeft == 0) 
                goto dlpop;
            DLOOP_Offset idx      = dl[sp].count - stack[sp].countLeft;
            inbase                = stack[sp].base + dl[sp].s.i_t.offsets[idx];
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            goto dlpush;
            break;
        }
        case DL_STRUCT: {
            DBG_PRINTF(stderr, "struct%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if (--stack[sp].countLeft == 0) {
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop; 
            }
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            inbase = stack[sp].base + dl[sp].s.s_t.offsets[idx];
            dl = dl[sp].s.s_t.dls[idx];
            goto dlpush;
            break;
        }
        case DL_CONTIGCHILD: {
            DBG_PRINTF(stderr, "cc%d_pop(%p, %ld)\n",
                       sp, inbase, dl[sp].count);
            if (--stack[sp].countLeft == 0) {
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop; 
            }
            goto dlpush;
            break;
        }
        case DL_VECTORFINAL: {
            DBG_PRINTF(stderr, "vf%d_pop(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            if(--stack[sp].countLeft == 0) {
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count = sizeleft/(dl[sp].s.v_t.blklen*dl[sp].s.v_t.oldsize);
            if(count > 0) {
                if(count > stack[sp].countLeft)
                    count = stack[sp].countLeft;
                DL_pack_vector((const void*)(stack[sp].base+idx*dl[sp].s.v_t.stride),
                               (void**)&outbase,
                               &dl[sp],
                               count);
                sizeleft -= count*dl[sp].s.v_t.blklen*dl[sp].s.v_t.oldsize;
                stack[sp].countLeft -= count;
                if(stack[sp].countLeft == 0) {
                    inbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx += count;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            inbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEXFINAL: {
            DBG_PRINTF(stderr, "bf%d_pop(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            if(--stack[sp].countLeft == 0) {
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }

            /* If there's still blocks left, try to do several of them */
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count = sizeleft / (dl[sp].s.bi_t.blklen * dl[sp].s.bi_t.oldsize);
            if(count > 0) {
                if(count > stack[sp].countLeft)
                    count = stack[sp].countLeft;
                DL_pack_blockindexed((const void*)stack[sp].base,
                                     (void**)&outbase,
                                     &dl[sp],
                                     idx,
                                     idx+count);
                sizeleft -= count*dl[sp].s.bi_t.blklen*dl[sp].s.bi_t.oldsize;
                stack[sp].countLeft -= count;
                if(stack[sp].countLeft == 0) {
                    inbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx += count;
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            inbase                = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEXFINAL: {
            DBG_PRINTF(stderr, "if%d_pop(%p, %ld, %ld, %ld)\n",
                      sp, inbase, dl[sp].count, dl[sp].size, sizeleft);
            if(--stack[sp].countLeft == 0) {
                inbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }

            /* If there are still blocks left, try to do several at once */
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count, copied;
            for(count = idx, copied = 0; 
                count < dl[sp].count
                    && copied + dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize <= sizeleft; 
                count++)
                copied += dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize;
            if(count > idx) {
                DL_pack_indexed((const void*)stack[sp].base,
                                (void**)&outbase,
                                &dl[sp],
                                idx,
                                count);
                sizeleft -= copied;
                stack[sp].countLeft -= (count-idx);
                if(stack[sp].countLeft == 0) {
                    inbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx = count;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            inbase = stack[sp].base + dl[sp].s.i_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_CONTIGFINAL: {
            DBG_PRINTF( stderr, "Invalid FINAL state in pop(%d)!\n", dl[sp].kind );
            MPIU_Assert(0);
            return -1;
        }
        case DL_RETURNTO: {
            DBG_PRINTF(stderr, "POP: RETURNTO[%d](%returnto=d)\n", sp,
                    dl[sp].returnto);
            int returnto = dl[sp].returnto;
            dl = stack[sp].prevdl;
            // We add 1 to sp because when we pop it, it will be decremented
            sp = returnto+1;
            goto dlpop;
            break;
        }
            /* By using an EXIT command on pop, we never need to check the stack
               pointer */
        case DL_EXIT: {
            DBG_PRINTF(stderr, "POP:EXIT[%d]\n", sp);
            state->sp = 0;
            state->partial = 0;
            *copy_p = outSize - sizeleft;
            return 0;
        }
        case DL_BOTTOM: {
            DBG_PRINTF(stderr, "POP:BOTTOM[%d] (Error!)\n", sp);
            MPIU_Assert(0);
            return -1;
        }
        }
    } while (1);
    /* No way to reach here? */
    return -1;
}


/********** UNPACK **************/

/*
  Return values:
  -1: Error
  0: Done
  1: Needs to continue; state in state_p
*/
int DL_unpack(const void *inPtr, void*outPtr, DLOOP_Offset outSize, 
              DLOOP_Dataloop *dl,
              DLOOP_Dataloop_state *state,
              DLOOP_Offset *copy_p,
              int isbuiltin)
{
    MPI_Offset inbase = (MPI_Offset)inPtr;
    MPI_Offset outbase = (MPI_Offset)outPtr;
    long sp = 0;
    MPI_Aint sizeLeft = outSize;

    /* Local variables */
    DLOOP_Dataloop_stackelm* stack = state->stack;
    /* If it's a resume, this will get set correctly later */
    DLOOP_Offset partial = 0;

    *copy_p = 0;

    /* This is a resume */
    if(state->sp != 0) {
        sp      = state->sp;
        outbase = state->stack[sp].base;
        partial = state->partial;
        
        /* NOTE: For builtin types, we create a dataloop as needed on the stack
           so we don't save it in the state because when we resume, the same 
           type will be recreated in segment_packunpack. */
        if(!isbuiltin)
            dl = state->dl;

        /* By construction, the last state pushed into the stack will always
         * be a CONTIGFINAL. Need to decrement the stack pointer to ensure 
         * that it is set to the right place by the immediate next call to 
         * dlpush */
        sp--;
        goto dlpush;
    }
  
    do {
    dlpush:
        sp++;
        switch (dl[sp].kind) {
        case DL_CONTIG: {
            DBG_PRINTF(stderr, "PUSH: CONTIG[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            goto dlpush;
        }
        case DL_VECTOR: {
            DBG_PRINTF(stderr, "PUSH: VECTOR[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = outbase;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            goto dlpush;
            break;
        }
        case DL_VECTOR1: {
            DBG_PRINTF(stderr, "PUSH: VECTOR1[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX: {
            DBG_PRINTF(stderr, "PUSH: BLOCKINDEX[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = outbase;
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            outbase              += dl[sp].s.bi_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX1: {
            DBG_PRINTF(stderr, "PUSH: BLOCKINDEX1[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            outbase              += dl[sp].s.bi_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_INDEX: {
            DBG_PRINTF(stderr, "PUSH: INDEX[%d]\n", sp);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp+1].base      = outbase;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[0];
            outbase              += dl[sp].s.i_t.offsets[0];
            goto dlpush;
            break;
        }
        case DL_STRUCT: {
            DBG_PRINTF(stderr, "PUSH: STRUCT[%d](absbase=%p)\n", sp, 
                    inbase);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;
            stack[sp].prevdl      = dl;
            outbase              += dl[sp].s.s_t.offsets[0];
            dl                    = dl[sp].s.s_t.dls[0];
            goto dlpush;
            break;
        }
        case DL_CONTIGCHILD: {
            DBG_PRINTF(stderr, "PUSH: CONTIGCHILD[%d]\n", sp);
            /* countLeft set by parent */
            /* base set by parent */
            goto dlpush;
        }
        case DL_VECTORFINAL: {
            DBG_PRINTF(stderr, "PUSH: VECFINAL[%d] (sizeLeft=%ld)\n", sp,
                    sizeLeft);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeLeft >= dl[sp].size) {
                DL_unpack_vector((const void**)&inbase,
                                 (void*)outbase,
                                 &dl[sp],
                                 dl[sp].count);
                stack[sp].countLeft = 0;
                sizeLeft -= dl[sp].size;
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
                break; /* to dlpop */
            }
            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count = sizeLeft / (dl[sp].s.v_t.blklen * dl[sp].s.v_t.oldsize);
            if (count > 0) {
                DL_unpack_vector((const void**)&inbase, 
                                 (void*)outbase,
                                 &dl[sp],
                                 count);
                sizeLeft -= count * dl[sp].s.v_t.blklen * dl[sp].s.v_t.oldsize;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            outbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEXFINAL: {
            DBG_PRINTF(stderr, "(u)PUSH: BLOCKINDEXFINAL[%d](sizeLeft=%ld)\n", 
                       sp, sizeLeft);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeLeft >= dl[sp].size) {
                DL_unpack_blockindexed((const void**)&inbase,
                                       (void*)stack[sp].base,
                                       &dl[sp],
                                       0, 
                                       dl[sp].count);
                stack[sp].countLeft = 0;
                sizeLeft -= dl[sp].size;
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
                break; /* to dlpop */
            }
            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count = sizeLeft / (dl[sp].s.bi_t.blklen * dl[sp].s.bi_t.oldsize);
            if (count > 0) {
                DL_unpack_blockindexed((const void**)&inbase,
                                       (void*)stack[sp].base, 
                                       &dl[sp],
                                       0, 
                                       count);
                sizeLeft -= count*dl[sp].s.bi_t.blklen*dl[sp].s.bi_t.oldsize;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count; 
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            outbase               = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEXFINAL: {
            DBG_PRINTF(stderr, "(u)PUSH: INDEXFINAL[%d](sizeLeft=%ld)\n", sp,
                    sizeLeft);
            stack[sp].base        = outbase;
            stack[sp].countLeft   = dl[sp].count;

            if (sizeLeft >= dl[sp].size) {
                DL_unpack_indexed((const void**)&inbase,
                                  (void*)stack[sp].base,
                                  &dl[sp],
                                  0, 
                                  dl[sp].count);
                stack[sp].countLeft = 0;
                sizeLeft -= dl[sp].size;
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
                break; /* to dlpop */
            }
            
            /* destination too small.  Determine how far we can go */
            DLOOP_Offset count, copied;
            for(count = 0, copied = 0; 
                copied + dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize <= sizeLeft; 
                count++)
                copied += dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize;
            if (count > 0) {
                DL_unpack_indexed((const void**)&inbase,
                                  (void*)stack[sp].base,
                                  &dl[sp],
                                  0, 
                                  count);
                sizeLeft -= copied;
                stack[sp].countLeft -= count;
            }
            DLOOP_Offset idx = count;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            outbase = stack[sp].base + dl[sp].s.i_t.offsets[idx];
            goto dlpush;
            break;
        }

        case DL_CONTIGFINAL: {
            DBG_PRINTF(stderr, "(u)PUSH: CONTIGFINAL[%d](sizeLeft=%ld)\n", 
                       sp, sizeLeft);

            stack[sp].base = outbase;

            /* Quick check to see if there's enough buffer space to copy. 
             * sizeLeft can be zero if the previous *FINAL type has blksize 
             * which is a multiple of in/outsize */
            if(sizeLeft == 0) {
                dlStackCopy(sp, dl, 0, state);
                *copy_p = outSize;
                return 1;
            }

            /* This case occurs when the contigfinal is directly below a contig
             * which is part of a struct. There ought to be a better way of
             * doing this, and eventually, I'll try and get rid of this check */
            if(stack[sp].countLeft == 0 && partial == 0)
                stack[sp].countLeft = dl[sp].count;

            DLOOP_Offset basesize = dl[sp].s.c_t.basesize;
            DLOOP_Offset tocopy = stack[sp].countLeft*basesize + partial;
            if(tocopy > sizeLeft) {
                DL_pack_contig((const void*)inbase,
                               (void*)outbase,
                               &dl[sp],
                               sizeLeft);
                DLOOP_Offset newLeft = stack[sp].countLeft;
                if(partial >= sizeLeft) {
                    partial -= sizeLeft;
                } else {
                    DLOOP_Offset avail = sizeLeft-partial;
                    newLeft -= (avail/basesize);
                    partial = 0;
                    if(avail%basesize) {
                        newLeft -= 1;
                        partial = basesize - avail%basesize;
                    }
                }
                stack[sp].base      = outbase + sizeLeft;
                stack[sp].countLeft = newLeft;
                dlStackCopy(sp, dl, partial, state);
                *copy_p = outSize;
                return 1;
            } else {
                DL_pack_contig((const void*)inbase, 
                               (void*)outbase,
                               &dl[sp],
                               tocopy);
                partial   = 0;
                inbase   += tocopy;
                outbase  += tocopy;
                sizeLeft -= tocopy;
                stack[sp].countLeft = 0;
                goto dlpop;
            }
            break;
        }

        default: {
            /* This can never happen */
            DBG_PRINTF(stderr, "The impossible happened(%d)!\n", dl[sp].kind);
            MPIU_Assert(0);
            return -1;
        }
        }

    dlpop:
        sp--;

        /* Question: can we ever pop INTO an xxFINAL? Or will those all
           be handled in the continue-from-state? */

        switch (dl[sp].kind) {
        case DL_CONTIG: {
            DBG_PRINTF(stderr, "POP: CONTIG[%d]\n", sp);
            if(--stack[sp].countLeft == 0) 
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + idx*dl[sp].s.c_t.baseextent;
            goto dlpush; 
            break;
        }
        case DL_VECTOR: {
            DBG_PRINTF(stderr, "POP: VEC[%d]\n", sp);
            /* For vectors, we don't need to compute offsets from the base.
             * The stride is the distance from each element, so we can use
             * the stack contents to update */
            if(--stack[sp].countLeft == 0) 
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            goto dlpush;
            break;
        }
        case DL_VECTOR1: {
            DBG_PRINTF(stderr, "POP: VECTOR1[%d]\n", sp);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX: {
            DBG_PRINTF(stderr, "POP: BLOCKINDEX[%d] (countLeft=%ld)\n", 
                       sp, stack[sp].countLeft);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEX1: {
            DBG_PRINTF(stderr, "POP: BLOCKINDEX1[%d]\n", sp);
            if(--stack[sp].countLeft == 0)
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEX: {
            DBG_PRINTF(stderr, "POP: INDEX[%d] (countLeft=%ld)\n", 
                       sp, stack[sp].countLeft);
            if(--stack[sp].countLeft == 0) 
                goto dlpop;
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            outbase = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_STRUCT: {
            DBG_PRINTF(stderr, "POP: STRUCT[%d] (countLeft=%ld)\n", 
                       sp, stack[sp].countLeft-1);
            if (--stack[sp].countLeft == 0) {
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop; 
            }
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            outbase = stack[sp].base + dl[sp].s.s_t.offsets[idx];
            dl = dl[sp].s.s_t.dls[idx];
            goto dlpush;
            break;
        }
        case DL_CONTIGCHILD: {
            DBG_PRINTF(stderr, "POP: CONTIGCHILD[%d](countLeft=%d)\n", sp,
                    stack[sp].countLeft-1);
            if (--stack[sp].countLeft == 0) {
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop; 
            }
            goto dlpush;
            break;
        }
        case DL_VECTORFINAL: {
            DBG_PRINTF(stderr, 
                    "POP: VECFINAL[%d](countLeft=%ld)\n", sp,
                    stack[sp].countLeft);
            if(--stack[sp].countLeft == 0) {
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count = sizeLeft / (dl[sp].s.v_t.blklen * dl[sp].s.v_t.oldsize);
            if(count > 0) {
                if(count > stack[sp].countLeft)
                    count = dl[sp].count - idx;
                DL_unpack_vector((const void**)&inbase,
                                 (void*)(stack[sp].base+idx*dl[sp].s.v_t.stride),
                                 &dl[sp],
                                 count);
                sizeLeft -= count*dl[sp].s.v_t.blklen*dl[sp].s.v_t.oldsize;
                stack[sp].countLeft -= count;
                if(stack[sp].countLeft == 0) {
                    outbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx += count;
            stack[sp+1].countLeft = dl[sp].s.v_t.blklen;
            outbase = stack[sp].base + idx*dl[sp].s.v_t.stride;
            goto dlpush;
            break;
        }
        case DL_BLOCKINDEXFINAL: {
            DBG_PRINTF(stderr, "POP: BLOCKINDEXFINAL[%d]\n", sp);
            if(--stack[sp].countLeft == 0) {
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }

            /* If there's still blocks left, try to do several of them */
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count = sizeLeft / (dl[sp].s.bi_t.blklen * dl[sp].s.bi_t.oldsize);
            if(count > 0) {
                if(count > stack[sp].countLeft)
                    count = dl[sp].count - idx;
                DL_unpack_blockindexed((const void**)&inbase,
                                       (void*)stack[sp].base,
                                       &dl[sp],
                                       idx,
                                       idx+count);
                sizeLeft -= count*dl[sp].s.bi_t.blklen*dl[sp].s.bi_t.oldsize;
                stack[sp].countLeft -= count;
                if(stack[sp].countLeft == 0) {
                    outbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx += count;
            stack[sp+1].countLeft = dl[sp].s.bi_t.blklen;
            outbase               = stack[sp].base + dl[sp].s.bi_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_INDEXFINAL: {
            DBG_PRINTF(stderr, "POP: INDEXFINAL[%d]\n", sp);
            if(--stack[sp].countLeft == 0) {
                outbase = stack[sp].base + dl[sp].extent;
                goto dlpop;
            }

            /* If there are still blocks left, try to do several at once */
            DLOOP_Offset idx = dl[sp].count - stack[sp].countLeft;
            DLOOP_Offset count = idx, copied = 0;
            for(count = idx, copied = 0;
                count < dl[sp].count
                    && copied + dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize <= sizeLeft; 
                count++)
                copied += dl[sp].s.i_t.blklens[count]*dl[sp].s.i_t.oldsize;
            if(count > idx) {
                DL_unpack_indexed((const void**)&inbase,
                                  (void*)stack[sp].base,
                                  &dl[sp],
                                  idx,
                                  count);
                sizeLeft -= copied;
                stack[sp].countLeft -= (count - idx);
                if(stack[sp].countLeft == 0) {
                    outbase = stack[sp].base + dl[sp].extent;
                    goto dlpop;
                }
            }
            idx = count;
            stack[sp+1].countLeft = dl[sp].s.i_t.blklens[idx];
            outbase = stack[sp].base + dl[sp].s.i_t.offsets[idx];
            goto dlpush;
            break;
        }
        case DL_CONTIGFINAL: {
            DBG_PRINTF( stderr, "Invalid FINAL state in pop(%d)!\n", dl[sp].kind );
            MPIU_Assert(0);
            return -1;
        }
        case DL_RETURNTO: {
            DBG_PRINTF(stderr, "POP: RETURNTO[%d](%returnto=%d)\n", sp,
                       dl[sp].returnto);
            int returnto = dl[sp].returnto;
            dl = stack[sp].prevdl;
            // We add 1 to sp because at dlpop it will be decremented
            sp = returnto+1;
            goto dlpop;
            break;
        }
            /* By using an EXIT command on pop, we never need to check the stack
               pointer */
        case DL_EXIT: {
            DBG_PRINTF(stderr, "POP:EXIT[%d]\n", sp);
            state->sp = 0;
            state->partial = 0;
            *copy_p = outSize - sizeLeft;
            return 0;
        }
        case DL_BOTTOM: {
            DBG_PRINTF(stderr, "POP:BOTTOM[%d] (Error!)\n", sp);
            MPIU_Assert(0);
            return -1;
        }
        }
    } while (1);
    /* No way to reach here? */
    return -1;
}

/* Segment_init
 *
 * buf    - datatype buffer location
 * count  - number of instances of the datatype in the buffer
 * handle - handle for datatype (could be derived or not)
 * segp   - pointer to previously allocated segment structure
 * flag   - flag indicating which optimizations are valid
 *          should be one of DLOOP_DATALOOP_HOMOGENEOUS, _HETEROGENEOUS,
 *          of _ALL_BYTES.
 *
 * Notes:
 * - Assumes that the segment has been allocated.
 * - Older MPICH code may pass "0" to indicate HETEROGENEOUS or "1" to
 *   indicate HETEROGENEOUS.
 *
 */
int PREPEND_PREFIX(Segment_init)(const DLOOP_Buffer buf,
                                 DLOOP_Count count,
                                 DLOOP_Handle handle,
                                 struct DLOOP_Segment *segp,
                                 int flag)
{
    segp->ptr = (DLOOP_Buffer) buf;
    segp->handle = handle;
    segp->count = count;
    segp->in_offset = 0;
    segp->out_offset = 0;
    segp->curcount = 0;

    segp->state.sp = 0;
    segp->state.dl = 0;
    segp->state.partial = 0;
    memset(segp->state.stack, 
           0, 
           DLOOP_MAX_DATALOOP_STACK*sizeof(DLOOP_Dataloop_stackelm));

	return 0;
}

/* Segment_alloc
 *
 */
struct DLOOP_Segment * PREPEND_PREFIX(Segment_alloc)(void)
{
    return (struct DLOOP_Segment*)DLOOP_Malloc(sizeof(struct DLOOP_Segment));
}

/* Segment_free
 *
 * Input Parameters:
 * segp - pointer to segment
 */
void PREPEND_PREFIX(Segment_free)(struct DLOOP_Segment *segp)
{
    DLOOP_Free(segp);
    return;
}
