/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#ifndef DATALOOP_PARTS_H
#define DATALOOP_PARTS_H

/* Check that all the appropriate defines are present */
#ifndef PREPEND_PREFIX
#error "PREPEND_PREFIX must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Offset
#error "DLOOP_Offset must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Count
#error "DLOOP_Count must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Handle
#error "DLOOP_Handle must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Buffer
#error "DLOOP_Buffer must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Type
#error "DLOOP_Type must be defined before dataloop_parts.h is included."
#endif

#ifndef DLOOP_Size
#error "DLOOP_Size must be defined before dataloop_parts.h is included."
#endif

/* Redefine all of the internal structures in terms of the prefix */
#define DLOOP_Dataloop              PREPEND_PREFIX(Dataloop)
#define DLOOP_Dataloop_contig       PREPEND_PREFIX(Dataloop_contig)
#define DLOOP_Dataloop_vector       PREPEND_PREFIX(Dataloop_vector)
#define DLOOP_Dataloop_blockindexed PREPEND_PREFIX(Dataloop_blockindexed)
#define DLOOP_Dataloop_indexed      PREPEND_PREFIX(Dataloop_indexed)
#define DLOOP_Dataloop_struct       PREPEND_PREFIX(Dataloop_struct)
#define DLOOP_Dataloop_stackelm     PREPEND_PREFIX(Dataloop_stackelm)


#define DLOOP_DATALOOP_PACKUNPACK_COMPLETED 0
#define DLOOP_DATALOOP_PACKUNPACK_PARTIAL 1
#define DLOOP_DATALOOP_PACKUNPACK_ERROR -1

 
typedef enum { 
    DL_BOTTOM          = -1, 
    DL_EXIT            = 0,
    
    DL_CONTIG          = 1,   
    DL_CONTIGFINAL     = 11,

    DL_VECTOR          = 2,   
    DL_VECTOR1         = 3, 
    DL_VECTORFINAL     = 21,
    DL_VECFINAL_1      = 22, 
    DL_VECFINAL_2      = 23, 
    DL_VECFINAL_4      = 24, 
    DL_VECFINAL_8      = 25,
    
    DL_BLOCKINDEX      = 4, 
    DL_BLOCKINDEX1     = 5,
    DL_BLOCKINDEXFINAL = 41,
    DL_BLKINDEXFINAL_1 = 42,
    DL_BLKINDEXFINAL_2 = 43,
    DL_BLKINDEXFINAL_4 = 44,
    DL_BLKINDEXFINAL_8 = 45,

    DL_INDEX           = 6,
    DL_INDEXFINAL      = 61,

    DL_STRUCT          = 7,

    DL_CONTIGCHILD     = 8, 

    DL_RETURNTO        = 31
} DLOOP_Dataloop_kind;

// Forward declaration
struct DLOOP_Dataloop;

typedef struct DLOOP_Dataloop_contig {
    MPI_Aint basesize;
    MPI_Aint baseextent;
} DLOOP_Dataloop_contig;

typedef struct DLOOP_Dataloop_vector { 
    MPI_Aint oldsize;
    MPI_Aint blklen;  /* This is the blocklength in #elements */
    MPI_Aint stride;
} DLOOP_Dataloop_vector;

typedef struct DLOOP_Dataloop_blockindexed {
    MPI_Aint oldsize;
    MPI_Aint blklen;
    const MPI_Aint *offsets;
} DLOOP_Dataloop_blockindexed;

typedef struct DLOOP_Dataloop_indexed {
    MPI_Aint oldsize;
    const MPI_Aint *blklens; 
    const MPI_Aint *offsets;
} DLOOP_Dataloop_indexed;

typedef struct DLOOP_Dataloop_struct {
    const MPI_Aint *oldsizes;
    const MPI_Aint *blklens;
    const MPI_Aint *offsets;
    struct DLOOP_Dataloop **dls;
} DLOOP_Dataloop_struct;

typedef struct DLOOP_Dataloop {
    DLOOP_Dataloop_kind kind;
    int          count;
    MPI_Aint     size;
    MPI_Aint     extent;
    /* This is used when operating on a struct type. It indicates where in the 
     * stack the control should return */
    int          returnto;
    /* Additional information about the dataloop, potentially to enable 
     * optimizations */
    unsigned     flags;
    union {
        DLOOP_Dataloop_contig       c_t;
        DLOOP_Dataloop_vector       v_t;
        DLOOP_Dataloop_blockindexed bi_t;
        DLOOP_Dataloop_indexed      i_t;
        DLOOP_Dataloop_struct       s_t;
    } s;
} DLOOP_Dataloop;

/* The max datatype depth is the maximum depth of the stack used to 
   evaluate datatypes.  It represents the length of the chain of 
   datatype dependencies.  Defining this and testing when a datatype
   is created removes a test in the datatype evaluation loop. */
#define DLOOP_MAX_DATATYPE_DEPTH 64
#define DLOOP_MAX_DATALOOP_STACK DLOOP_MAX_DATATYPE_DEPTH
typedef struct DLOOP_Dataloop_stackelm {
    /* This is the base address used to keep track of 
     * the src pointer when packing and the dst pointer when unpacking */
    MPI_Offset base;
    /* When we encounter a struct type, we push the current dataloop here
     * and use the dataloop of the struct element being processed */
    DLOOP_Dataloop *prevdl;
    /* For the CONTIGFINAL type, this contains the number of complete elements 
     * that are left to be copied (packed/unpacked). 
     * bytes = stack[i].countLeft*c_t.basesize + state.partial;
     * is the number of bytes remaining in the type to be processed
     * For all other types, this is simply the number of elements left
     */
    MPI_Aint countLeft;
} DLOOP_Dataloop_stackelm;

typedef struct DLOOP_Dataloop_state {
    /* Pointer in dataloop stack to which to resume */
    long sp;
    /* Actual dataloop of type to resume */
    DLOOP_Dataloop *dl;
    /* For the CONTIGFINAL type, this gives the number of bytes in a partially
     * copied element that are yet to be copied */
    DLOOP_Offset partial;
    DLOOP_Dataloop_stackelm stack[DLOOP_MAX_DATALOOP_STACK];
} DLOOP_Dataloop_state;


/* Optimization flags */
#define DLOOP_OPT_FLAG_ALIGNED 0x1
#define DLOOP_OPT_FLAG_ISSHORT 0x2

/* When copying blocks larger than this, use memcpy. 
 * TODO: Provide a way of configuring this useing ./configure */
#define DLOOP_MEMCPY_THRESHOLD 1024

/* Helper macros */
#define DLOOP_Dataloop_is_contig(dl)                    \
    ((dl)[1].kind == DL_CONTIGFINAL)                    \
    
#define set_dlflag(fvec, f)                   \
    ((fvec) | ~(f))                           \
    
#define get_dlflag(fvec, f)                   \
    ((fvec) & (f))                            \
    
#define DLOOP_opt_get_aligned(dl) get_dlflag((dl).flags, DLOOP_OPT_FLAG_ALIGNED)
#define DLOOP_opt_set_aligned(dl) set_dlflag((dl).flags, DLOOP_OPT_FLAG_ALIGNED)

#define DLOOP_opt_get_isshort(dl) get_dlflag((dl).flags, DLOOP_OPT_FLAG_ISSHORT)
#define DLOOP_opt_set_isshort(dl) set_dlflag((dl).flags, DLOOP_OPT_FLAG_ISSHORT)


/* --------------------------------------------------------------------------*/
/* TODO: TLP: Miscellaneous dataloop functions some of which we don't need */

/* Dataloop functions (dataloop.c) */
void PREPEND_PREFIX(Dataloop_copy)(void *dest,
				   void *src,
				   DLOOP_Size size);
void PREPEND_PREFIX(Dataloop_update)(DLOOP_Dataloop *dataloop,
				     DLOOP_Offset ptrdiff);
DLOOP_Offset
PREPEND_PREFIX(Dataloop_stream_size)(DLOOP_Dataloop *dl_p,
				     DLOOP_Offset (*sizefn)(DLOOP_Type el_type));
void PREPEND_PREFIX(Dataloop_print)(DLOOP_Dataloop *dataloop);

void PREPEND_PREFIX(Dataloop_alloc)(DLOOP_Dataloop **new_loop_p);
void PREPEND_PREFIX(Dataloop_alloc_and_copy)(DLOOP_Dataloop *old_loop,
    DLOOP_Dataloop **new_loop_p);
void PREPEND_PREFIX(Dataloop_struct_alloc)(DLOOP_Count count,
                                           DLOOP_Dataloop *dl);
void PREPEND_PREFIX(Dataloop_dup)(DLOOP_Dataloop *old_loop,
                                  DLOOP_Dataloop **new_loop_p);

void PREPEND_PREFIX(Dataloop_free)(DLOOP_Dataloop **dataloop);
void PREPEND_PREFIX(Dataloop_move)(DLOOP_Dataloop *old, DLOOP_Dataloop *new);


/* --------------------------------------------------------------------------*/
/* TODO: Everything below has to do with segments and we will get rid of it */

#define DLOOP_Segment               PREPEND_PREFIX(Segment)


/* NOTE: ASSUMING LAST TYPE IS SIGNED */
#define SEGMENT_IGNORE_LAST ((DLOOP_Offset) -1)


/*S
  DLOOP_Segment - Description of the Segment datatype

  Notes:
  This has no corresponding MPI datatype.

  Module:
  Segment

  Questions:
  Should this have an id for allocation and similarity purposes?
  S*/
typedef struct DLOOP_Segment { 
    void *ptr; /* pointer to datatype buffer */
    DLOOP_Handle handle;
    DLOOP_Offset in_offset;
    DLOOP_Offset out_offset;
    DLOOP_Count count;
    DLOOP_Count curcount;	
	/* TODO: This might cause problems when integrating into existing MPICH installations
	 * for which we don't have the netmod */
    DLOOP_Dataloop_state state;
} DLOOP_Segment;

/* Segment functions (segment.c) */
DLOOP_Segment * PREPEND_PREFIX(Segment_alloc)(void);

void PREPEND_PREFIX(Segment_free)(DLOOP_Segment *segp);

int PREPEND_PREFIX(Segment_init)(const DLOOP_Buffer buf,
				 DLOOP_Count count,
				 DLOOP_Handle handle,
				 DLOOP_Segment *segp,
				 int hetero);

int DL_pack(const void *inPtr, void*outPtr, DLOOP_Offset outSize, 
            DLOOP_Dataloop *dl,
            DLOOP_Dataloop_state *state_p,
            DLOOP_Offset *copy_p,
            int isbuiltin);

int DL_unpack(const void *inPtr, void*outPtr, DLOOP_Offset outSize, 
              DLOOP_Dataloop *dl,
              DLOOP_Dataloop_state *state_p,
              DLOOP_Offset *copy_p,
              int isbuiltin);


/* Common segment operations (segment_ops.c) */
void PREPEND_PREFIX(Segment_count_contig_blocks)(DLOOP_Segment *segp,
						 DLOOP_Offset first,
						 DLOOP_Offset *lastp,
						 DLOOP_Count *countp);


void PREPEND_PREFIX(Segment_pack)(struct DLOOP_Segment *segp,
				  DLOOP_Offset   first,
				  DLOOP_Offset  *lastp,
				  void *streambuf);
void PREPEND_PREFIX(Segment_unpack)(struct DLOOP_Segment *segp,
				    DLOOP_Offset   first,
				    DLOOP_Offset  *lastp,
				    void *streambuf);

#endif

