/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "dataloop.h"
#include "veccpy.h"

/* #define MPICH_DEBUG_SEGMENT_MOVE */
/* TODO: Consider integrating this with the general debug support. */
/* Note: This does not use the CVAR support for the environment variable
   because (a) this is a temporary code and (b) it is expert developer
   only */
#ifdef MPICH_DEBUG_SEGMENT_MOVE
static int printSegment = -1;
static void setPrint( void ) {
    char *s = getenv( "MPICH_DATALOOP_PRINT" );
    if (s && (strcmp(s,"yes")==0 || strcmp(s,"YES") == 0)) {
        printSegment = 1;
    }
    else {
        printSegment = 0;
    }
}
#define DBG_SEGMENT(_a) do { if (printSegment < 0) setPrint(); \
        if (printSegment) { _a; } } while( 0 )
#else
#define DBG_SEGMENT(_a)
#endif


void PREPEND_PREFIX(Segment_pack)(DLOOP_Segment *segp,
                                  DLOOP_Offset   first,
                                  DLOOP_Offset  *lastp,
                                  void *streambuf)
{
    /* TODO: TLP: Use max(size_t) */
    size_t outSize = LONG_MAX;
    if(*lastp != SEGMENT_IGNORE_LAST)
        outSize = (*lastp - first);

    DLOOP_Offset size, extent;
    DLOOP_Handle_get_size_macro(segp->handle, size);
    DLOOP_Handle_get_extent_macro(segp->handle, extent);

    DLOOP_Offset totalcopied = 0;

    DLOOP_Dataloop *dl = NULL;
    DLOOP_Handle_get_loopptr_macro(segp->handle, dl);

#ifdef WITH_JIT
    MPID_Datatype *dt_ptr;
    MPID_Datatype_get_ptr(segp->handle, dt_ptr);
    /* TODO: When LLVM is used and it's a builtin type, shouldn't have to
     * revert to the interpreted engine */
#endif

    if(!dl) {
        /* TODO: If no dataloop is defined, it should be a basic type for which
         * size == extent. Need to confirm this though */
        MPIU_Assert(size == extent);
        DLOOP_Dataloop defDL[3];
        defDL[0].kind = DL_EXIT;
        defDL[1].kind = DL_CONTIGFINAL; 
        defDL[1].count = segp->count;
        defDL[1].size = size*segp->count;
        defDL[1].extent = extent;
        defDL[1].s.c_t.basesize = size;
        defDL[1].s.c_t.baseextent = extent;
        defDL[2].kind = DL_BOTTOM;

        DL_pack(segp->ptr+segp->in_offset, streambuf, outSize, defDL, &segp->state, &totalcopied, 1);
        segp->curcount = totalcopied/size;
        segp->in_offset += segp->curcount*size;

        goto exit;
    }

    int i;
    for(i = segp->curcount; i < segp->count; i++, segp->curcount++) {
        int rv;
        DLOOP_Offset copied = 0;
        DLOOP_Offset outsz = outSize - totalcopied;

#ifdef WITH_JIT
        if(dt_ptr->dljit_ee) {
            rv = dt_ptr->dljit_fn_pack(segp->ptr+segp->in_offset, 
                                       streambuf+totalcopied, 
                                       outsz,
                                       &segp->state,
                                       &copied);
        } else {
            rv = DL_pack(segp->ptr+segp->in_offset, 
                         streambuf+totalcopied, 
                         outsz,
                         dl, 
                         &segp->state,
                         &copied,
                         0);
        }
#else
        rv = DL_pack(segp->ptr+segp->in_offset, 
                     streambuf+totalcopied, 
                     outsz,
                     dl, 
                     &segp->state,
                     &copied,
                     0);
#endif

        totalcopied += copied;
        /* TODO: TLP: Use the MPICH error handling */
        switch(rv) {
        case DLOOP_DATALOOP_PACKUNPACK_COMPLETED: break;
        case DLOOP_DATALOOP_PACKUNPACK_PARTIAL: goto exit; break;
        default:
            fprintf(stderr, "*** ERROR in pack(%d)\n", rv);
            MPIU_Assert(0);
        }

        segp->in_offset += extent;
    }

 exit:
    *lastp = first + totalcopied;
    return;
}

void PREPEND_PREFIX(Segment_unpack)(DLOOP_Segment *segp,
                                    DLOOP_Offset   first,
                                    DLOOP_Offset  *lastp,
                                    void *streambuf)
{
    /* TODO: TLP: Use max(size_t) */
    size_t inSize = LONG_MAX;
    if(*lastp != SEGMENT_IGNORE_LAST)
        inSize = (*lastp - first);

    DLOOP_Offset size, extent;
    DLOOP_Handle_get_size_macro(segp->handle, size);
    DLOOP_Handle_get_extent_macro(segp->handle, extent);

    DLOOP_Offset totalcopied = 0;

    DLOOP_Dataloop *dl;
    DLOOP_Handle_get_loopptr_macro(segp->handle, dl);

#ifdef WITH_JIT
    MPID_Datatype *dt_ptr;
    MPID_Datatype_get_ptr(segp->handle, dt_ptr);
    /* TODO: When LLVM is used and it's a builtin type, shouldn't have to
     * revert to the interpreted engine */
#endif

    if(!dl) {
        /* If no dataloop is defined, it should be a basic type for which
         * size == extent. Need to confirm this though */
        MPIU_Assert(size == extent);
        DLOOP_Dataloop defDL[3];
        defDL[0].kind = DL_EXIT;
        defDL[1].kind = DL_CONTIGFINAL;
        defDL[1].count = segp->count;
        defDL[1].size = size*segp->count;
        defDL[1].extent = extent;
        defDL[1].s.c_t.basesize = size;
        defDL[1].s.c_t.baseextent = extent;
        defDL[2].kind = DL_BOTTOM;

        DL_unpack(streambuf, segp->ptr+segp->out_offset, inSize, defDL, &segp->state, &totalcopied, 1);
        segp->curcount = totalcopied/size;
        segp->out_offset += segp->curcount*extent;
        
        goto exit;
    }

    int i;
    for(i = segp->curcount; i < segp->count; i++, segp->curcount++) {
        int rv;
        DLOOP_Offset copied = 0;
        DLOOP_Offset insz = inSize - totalcopied;
#ifdef WITH_JIT
        if(dt_ptr->dljit_ee) {
            rv = dt_ptr->dljit_fn_unpack(streambuf+totalcopied, 
                                         segp->ptr+segp->out_offset,
                                         insz,
                                         &segp->state,
                                         &copied);
        } else {
            rv = DL_unpack(streambuf+totalcopied, 
                           segp->ptr+segp->out_offset,
                           insz,
                           dl, 
                           &segp->state,
                           &copied,
                           0);
        }
#else 
        rv = DL_unpack(streambuf+totalcopied, 
                       segp->ptr+segp->out_offset,
                       insz,
                       dl, 
                       &segp->state,
                       &copied,
                       0);
#endif

        totalcopied += copied;
        /* TODO: TLP: Use the MPICH error handling */
        switch(rv) {
        case DLOOP_DATALOOP_PACKUNPACK_COMPLETED: break;
        case DLOOP_DATALOOP_PACKUNPACK_PARTIAL: goto exit; break;
        default:
            fprintf(stderr, "*** ERROR in unpack(%d)\n", rv);
            MPIU_Assert(0);
        }

        segp->out_offset += extent;
    }
 exit:    
    *lastp = first + totalcopied;
    return;
}

