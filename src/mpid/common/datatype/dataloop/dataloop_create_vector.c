/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include "./dataloop.h"

/*@
  Dataloop_create_vector

  Arguments:
  +  int icount
  .  int iblocklength
  .  MPI_Aint astride
  .  int strideinbytes
  .  MPI_Datatype oldtype
  .  DLOOP_Dataloop **dlp_p
  .  int *dlsz_p
  .  int *dldepth_p
  -  int flag

  Returns 0 on success, -1 on failure.

  @*/
int PREPEND_PREFIX(Dataloop_create_vector)(DLOOP_Count count,
                                           DLOOP_Size blocklength,
                                           MPI_Aint stride,
                                           int strideinbytes,
                                           MPI_Datatype type,
                                           DLOOP_Type oldtype,
                                           DLOOP_Dataloop **dl,
                                           MPI_Aint *size,
                                           int *depth)
{
    if(!*dl) {
        MPIU_Assert(*depth == -1);
        PREPEND_PREFIX(Dataloop_alloc)(dl);
        *depth = 1;
    }

    /* if count or blocklength are zero, handle with contig code,
     * call it a int
     */
    if (count == 0 || blocklength == 0) {
        int err;
        err = PREPEND_PREFIX(Dataloop_create_contiguous)(0,
                                                         MPI_INT,
                                                         dl,
                                                         size,
                                                         depth);
        return err;
    }

    /* optimization:
     *
     * if count == 1, store as a contiguous rather than a vector dataloop.
     */
    if (count == 1) {
        int err;
        err = PREPEND_PREFIX(Dataloop_create_contiguous)(blocklength,
                                                         oldtype,
                                                         dl,
                                                         size,
                                                         depth);
        return err;
    }

    int is_builtin;
    DLOOP_Size oldsize = 0, oldextent = 0;
    DLOOP_Handle_get_size_macro(oldtype, oldsize);
    DLOOP_Handle_get_extent_macro(oldtype, oldextent);

    MPID_Datatype *oldtype_ptr;
    MPID_Datatype_get_ptr(oldtype, oldtype_ptr);

    MPI_Aint typesize, typeextent;
    DLOOP_Handle_get_size_macro(type, typesize);
    DLOOP_Handle_get_extent_macro(type, typeextent);
 
    is_builtin = (DLOOP_Handle_hasloop_macro(oldtype)) ? 0 : 1;
    
    (*dl)[*depth].kind   = DL_VECTOR;
    (*dl)[*depth].count  = count;
    (*dl)[*depth].size   = typesize;
    (*dl)[*depth].extent = typeextent;
    (*dl)[*depth].s.v_t.stride  = strideinbytes ? stride : stride*oldextent;
    (*dl)[*depth].s.v_t.blklen = blocklength;
    (*dl)[*depth].s.v_t.oldsize = oldsize;

    if(!is_builtin) {
        PREPEND_PREFIX(Dataloop_create)(oldtype, 
                                        &oldtype_ptr->dataloop,
                                        &oldtype_ptr->dataloop_size,
                                        &oldtype_ptr->dataloop_depth);
    }
    
    if(is_builtin || DLOOP_Dataloop_is_contig(oldtype_ptr->dataloop)) {
        (*dl)[*depth].kind = DL_VECTORFINAL;
        if(stride % oldsize == 0)
            DLOOP_opt_set_aligned((*dl)[*depth]);
        if(blocklength < DLOOP_MEMCPY_THRESHOLD)
            DLOOP_opt_set_isshort((*dl)[*depth]);

        *depth += 1;
        (*dl)[*depth].kind = DL_CONTIGFINAL;
        (*dl)[*depth].size = blocklength * oldsize;
        (*dl)[*depth].extent = blocklength * oldextent;
        (*dl)[*depth].s.c_t.basesize = oldsize;
        (*dl)[*depth].s.c_t.baseextent = oldextent;
    } else {
        if(blocklength > 1) {
            *depth += 1;
            (*dl)[*depth].kind = DL_CONTIGCHILD;
            (*dl)[*depth].size = oldsize;
            (*dl)[*depth].extent = typeextent;
        } else {
            (*dl)[*depth].kind = DL_VECTOR1;
        }

        *depth += 1;
        /* The first element of the innerdl will be DL_EXIT */
        DLOOP_Dataloop *pos = &(*dl)[*depth];
        PREPEND_PREFIX(Dataloop_dup)(&oldtype_ptr->dataloop[1], &pos);
        /* The last element will be DL_BOTTOM */
        *depth = *depth + oldtype_ptr->dataloop_depth - 1;
    }
  
    
    return 0;
}

